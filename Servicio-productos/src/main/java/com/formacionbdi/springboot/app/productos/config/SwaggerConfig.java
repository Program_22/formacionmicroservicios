package com.formacionbdi.springboot.app.productos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	  public Docket api() {
	    return new Docket(DocumentationType.SWAGGER_2)
	        .apiInfo(swaggerApiInformation())
	        .select().paths(PathSelectors.any())
	        .build();
	  }

	  private ApiInfo swaggerApiInformation() {
	    return new ApiInfoBuilder().title("Productos")
	        .description("Spring BOOT")
	        .termsOfServiceUrl("https://gitlab.com/Program_22/formacionmicroservicios")
	        .contact(
	            new Contact("Alexander Estela", "https://gitlab.com/aestela", "aestelah@everis.com"))
	        .version("1.5").build();
	  }
}
