package com.formacionbdi.springboot.app.productos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.formacionbdi.springboot.app.productos.models.entity.Producto;
import com.formacionbdi.springboot.app.productos.models.service.ProductoService;

@RestController
public class ProductoController {

	@Autowired
	ProductoService service;

	@PostMapping("/producto/crear")
	public ResponseEntity<Producto> postProducto(@RequestBody Producto producto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(producto));
	}

	@GetMapping("/producto/listar")
	public ResponseEntity<List<Producto>> getProducto() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/producto/ver/{Id}")
	public ResponseEntity<Producto> getProducto(@PathVariable Long Id) {
		return ResponseEntity.status(HttpStatus.OK).body(service.findById(Id));
	}

	@PutMapping("/producto/editar/{Id}")
	public ResponseEntity<Producto> patchProducto(@PathVariable Long Id, @RequestBody Producto producto) {
		return ResponseEntity.status(HttpStatus.OK).body(service.update(Id, producto));
	}

	@DeleteMapping("/producto/eliminar/{Id}")
	public ResponseEntity<Void> deleteProducto(@PathVariable Long Id) {
		service.deleteById(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
