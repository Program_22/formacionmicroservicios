package com.formacionbdi.springboot.app.productos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.formacionbdi.springboot.app.productos.models.entity.Categoria;
import com.formacionbdi.springboot.app.productos.models.service.CategoriaService;

@RestController
public class CategoriaController {

	@Autowired
	CategoriaService service;

	@PostMapping("/categoria/crear")
	public ResponseEntity<Categoria> postCategoria(@RequestBody Categoria categoria) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(categoria));
	}

	@GetMapping("/categoria/listar")
	public ResponseEntity<List<Categoria>> getCategoria() {
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}

	@GetMapping("/categoria/ver/{Id}")
	public ResponseEntity<Categoria> getCategoria(@PathVariable Long Id) {
		return ResponseEntity.status(HttpStatus.OK).body(service.findById(Id));
	}

	@PutMapping("/categoria/editar/{Id}")
	public ResponseEntity<Categoria> patchCategoria(@PathVariable Long Id, @RequestBody Categoria categoria) {
		return ResponseEntity.status(HttpStatus.OK).body(service.update(Id, categoria));
	}

	@DeleteMapping("/categoria/eliminar/{Id}")
	public ResponseEntity<Void> deleteProducto(@PathVariable Long Id) {
		service.deleteById(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
