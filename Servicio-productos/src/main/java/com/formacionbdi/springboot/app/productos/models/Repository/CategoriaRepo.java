package com.formacionbdi.springboot.app.productos.models.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacionbdi.springboot.app.productos.models.entity.Categoria;

@Repository
public interface CategoriaRepo extends JpaRepository<Categoria, Long>{

}
