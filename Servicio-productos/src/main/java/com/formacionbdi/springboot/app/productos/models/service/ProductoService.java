package com.formacionbdi.springboot.app.productos.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.springboot.app.productos.models.Repository.ProductoRepo;
import com.formacionbdi.springboot.app.productos.models.entity.Producto;

@Service
@Transactional
public class ProductoService {

	@Autowired
	ProductoRepo Repo;

	public List<Producto> findAll() {
		return (List<Producto>) Repo.findAll();
	}

	public Producto findById(Long id) {
		return Repo.findById(id).orElse(null);
	}

	public Producto save(Producto producto) {
		return Repo.save(producto);
	}

	public Producto update(Long id, Producto producto) {

		return Repo.save(producto);

	}

	public void deleteById(Long id) {
		Repo.deleteById(id);
	}
}
