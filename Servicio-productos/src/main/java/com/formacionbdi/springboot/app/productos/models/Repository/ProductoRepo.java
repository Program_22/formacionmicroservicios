package com.formacionbdi.springboot.app.productos.models.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.formacionbdi.springboot.app.productos.models.entity.Producto;
@Repository
public interface ProductoRepo extends JpaRepository<Producto, Long>{

}
