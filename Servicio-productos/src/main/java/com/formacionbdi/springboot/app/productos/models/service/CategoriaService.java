package com.formacionbdi.springboot.app.productos.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.springboot.app.productos.models.Repository.CategoriaRepo;
import com.formacionbdi.springboot.app.productos.models.entity.Categoria;

@Service
@Transactional
public class CategoriaService {

	@Autowired
	CategoriaRepo Repo;

	public List<Categoria> findAll() {
		return (List<Categoria>) Repo.findAll();
	}

	public Categoria findById(Long id) {
		return Repo.findById(id).orElse(null);
	}

	public Categoria save(Categoria categoria) {
		return Repo.save(categoria);
	}

	public Categoria update(Long i, Categoria categoria) {
		return Repo.save(categoria);
	}

	public void deleteById(Long id) {
		Repo.deleteById(id);
	}

}
