package com.example.app.repository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.example.app.documents.Categoria;
import reactor.core.publisher.Mono;
@Repository
public interface CategoriaRepo extends ReactiveMongoRepository<Categoria,String>{
	public Mono<Categoria> findByNombre(String nombre);
}
