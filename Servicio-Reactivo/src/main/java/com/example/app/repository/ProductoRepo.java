package com.example.app.repository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.example.app.documents.Producto;

import reactor.core.publisher.Mono;

public interface ProductoRepo extends ReactiveMongoRepository<Producto, String>{

	public Mono<Producto> findByNombre(String nombre);
	
	@Query("{ 'nombre': ?0 }")
	public Mono<Producto> obtenerPorNombre(String nombre);
	

}
