package com.example.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.app.documents.Categoria;
import com.example.app.documents.Producto;
import com.example.app.repository.CategoriaRepo;
import com.example.app.repository.ProductoRepo;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class ProductoService {

	@Autowired
ProductoRepo Repo;
	@Autowired
	CategoriaRepo Repoc;


	public Flux<Producto> findAll() {
		return Repo.findAll();
	}


	public Mono<Producto> findById(String id) {
		return Repo.findById(id);
	}


	public Mono<Producto> save(Producto producto) {
		return Repo.save(producto);
	}


	public Mono<Void> delete(Producto producto) {
		return Repo.delete(producto);
	}


	public Flux<Producto> findAllConNombreUpperCase() {
		return Repo.findAll().map(producto -> {
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		});
	}


	public Flux<Producto> findAllConNombreUpperCaseRepeat() {
		return findAllConNombreUpperCase().repeat(5000);
	}


	public Flux<Categoria> findAllCategoria() {
		return Repoc.findAll();
	}


	public Mono<Categoria> findCategoriaById(String id) {
		return Repoc.findById(id);
	}


	public Mono<Categoria> saveCategoria(Categoria categoria) {
		return Repoc.save(categoria);
	}

/*
	public Mono<Producto> findByNombre(String nombre) {
		return Repo.obtenerPorNombre(nombre);
	}

*/
	public Mono<Categoria> findCategoriaByNombre(String nombre) {
		return Repoc.findByNombre(nombre);
	}
}
