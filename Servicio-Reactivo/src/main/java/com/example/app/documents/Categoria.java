package com.example.app.documents;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Document(collection = "categorias")
public class Categoria {
	@Id
	@NotEmpty
	private String id;
	private String nombre;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createAt;
	public Categoria() {

	}

	
	public Categoria(String nombre) {

		this.nombre = nombre;
	}


	public Categoria(String nombre, Date createAt) {
		
		this.nombre = nombre;
		this.createAt = createAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	
	
}
