package com.example.item.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {
	@Bean("clienteRest")
	@LoadBalanced
	public RestTemplate registro() {
		return new RestTemplate();
	}
	

}
